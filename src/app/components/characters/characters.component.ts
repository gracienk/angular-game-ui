import { Component, OnInit } from '@angular/core';
import { CharacterService } from '../../services/character.service';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.css']
})
export class CharactersComponent implements OnInit {

  public characters;

  constructor(private characterService: CharacterService) { }

  ngOnInit(): void {
    this.getCharacters();
  }


  getCharacters(){
    this.characterService.getCharacters().subscribe(
      data => { this.characters = data; },
      err => { console.error(err); },
      () => { console.log('characters loaded'); }
    );
  }

}
